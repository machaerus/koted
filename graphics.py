#!/usr/bin/env python
"""
Download and process images to include in the presentation.
"""
import os
from typing import List

import requests


def download_random_image(keywords: List[str]):
	"""
	Download a random image from Unsplash based on the keywords.

	Parameters
	----------
	keywords : List[str]
		List of keywords to use as a query.

	Returns
	-------
	fname : str
		Path to the downloaded and saved file.
	"""
	unsplash_key = os.getenv("unsplash_key")
	base_url = "https://api.unsplash.com/photos/random?query={q}&client_id={key}"

	# Get a download link of a random image using Unsplash API.
	url = base_url.format(q=",".join(keywords), key=unsplash_key)
	res = requests.get(url)
	res.raise_for_status()

	# Download and save the image.
	download_link = res.json()['links']['download']
	image = requests.get(download_link)
	image.raise_for_status()

	fname = f"images/{'_'.join(keywords)}.jpg"
	with open(fname, 'wb') as f:
		f.write(image.content)

	return fname
