.PHONY: help
help:
	@echo "See Makefile for available commands!"

.PHONY: run
run:
	(set -a && source ./.env && poetry run python koted.py)
