#!/usr/bin/env python
"""
Set of methods for rendering Kotekian Latex Beamer presentations.
"""
import os
import re
import shlex
import subprocess
from typing import List

import jinja2

TEX_DIR = "tex"
PDF_DIR = "pdf"


class Presentation:
	"""
	Model of a kotekian presentation, holding its contents.

	For more convenient use in a Jinja2 template.
	"""

	class Slide:
		"""Model of a single slide."""
		def __init__(self, title: str, points: List[str], image: str = None):
			"""
			"""
			self.title = title
			self.points = points
			self.image = image

	def __init__(self, title: str):
		"""
		Create a new presentation.

		TODO: Add more metadata (subtitle, author, institute).

		Parameters
		----------
		title : str
			Title of the presentation.
		"""
		self.title = title
		self.__slides = []

	def add(self, title: str, points: List[str], image: str = None):
		"""
		Add a new slide to the presentation by defining its contents.

		Parameters
		----------
		title : str
			Title of the slide.
		points : List[str]
			List of sentences/paragraphs to be presented as bullet points.
		image : str
			Optional filename of an image to embed into the slide.
		"""
		slide = self.Slide(title, points, image)
		self.__slides.append(slide)

	def __iter__(self):
		"""Make the object iterable over slides' list."""
		for slide in self.__slides:
			yield slide


def normalize_title(title: str) -> str:
	"""
	Transform an arbitrary string to a machine-friendly UNIX file name.
	"""
	title = re.sub(r"[^0-9a-zA-Z\s]+", "-", title)
	title = title.lower()
	title = re.sub(r"\b(a|an|the)\b", " ", title)
	title = title.strip()
	title = re.sub(r"\s+", "_", title)
	return title


def generate_tex(presentation: Presentation) -> str:
	"""
	Generate TEX code from a list "slides".

	Each slide is a list of sentences. First sentence is assumed to be a title of
	the slide, and the remaining sentences will be represented as bullet points
	in the slide, with a few special cases.

	If a sentence is a filename, the corresponding image will be used in the slide.
	If there is only one sentence, it will be rendered as a fullscreen motto instead
	of a bullet point.

	Parameters
	----------
	presentation : Presentation
		A Presentation instance holding the contents to be rendered.

	Returns
	-------
	fname : str
		Name of the output TEX file (without path or extension).
	"""
	jinja_env = jinja2.Environment(
		block_start_string=r'\BLOCK{',
		block_end_string='}',
		variable_start_string=r'\VAR{',
		variable_end_string='}',
		comment_start_string=r'\#{',
		comment_end_string='}',
		line_statement_prefix='%%',
		line_comment_prefix='%#',
		trim_blocks=True,
		autoescape=False,
		loader=jinja2.FileSystemLoader(os.path.abspath('.'))
	)
	t = jinja_env.get_template("template.jinja2")
	tex = t.render(presentation=presentation)
	fname = normalize_title(presentation.title)
	with open(f"{TEX_DIR}/{fname}.tex", 'w', encoding="utf-8") as f:
		f.write(tex)

	return fname


def render_pdf(fname: str) -> bytes:
	"""
	Render PDF from a TEX file.

	Parameters
	----------
	fname : str
		File name of the TEX file to be rendered (without
		path or extension).

	Returns
	-------
	_ : str
		Log output of pdflatex (either stdout or stderr).
	"""
	command = f"pdflatex -interaction=nonstopmode " \
		f"-output-directory='{PDF_DIR}' {TEX_DIR}/{fname}.tex"
	proc = subprocess.Popen(
		shlex.split(command),
		stdout=subprocess.PIPE,
		stderr=subprocess.PIPE)
	stdout, stderr = proc.communicate()

	try:
		fsize = os.path.getsize(f"{PDF_DIR}/{fname}.pdf")
		assert fsize > 100
		assert len(stderr.decode()) == 0
	except (FileNotFoundError, AssertionError) as e:
		print("Rendering PDF failed!")
		print(e)
		return stderr
	else:
		print("PDF generated successfully!")
		return stdout
