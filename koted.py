#!/usr/bin/env python
"""
Kotekian presentation generator.
"""
import random
import re
from typing import List, Tuple, Callable

from aitextgen import aitextgen
from bs4 import BeautifulSoup
import requests
import spacy
from spacy.symbols import NOUN, VERB, PROPN
from spacy.tokens.doc import Doc
from spacy.tokens.span import Span

import beamer
import graphics


def topic_generator() -> Tuple[str, str]:
	"""
	Find and emit a presentation topic.

	Yields
	------
	title : str
		Name of the topic.
	short : str
		Short summary of the topic.
	"""
	# Source of topics.
	wiki_url = "https://en.wikipedia.org/wiki/Special:Random"

	def get_topic() -> Tuple[str, str]:
		"""Get a random topic from Wikipedia."""
		r = requests.get(wiki_url)
		soup = BeautifulSoup(r.content, "html.parser")
		title_container = soup.find(id="firstHeading")
		title = title_container.text
		for p in soup.find_all("p"):
			if len(p) > 5:
				break
		short = p.text.strip()
		return title, short

	def validate(title: str) -> bool:
		"""Decide if the generated title is ok."""
		requirements = (
			len(title.split(" ")) > 2,
			~title.lower().startswith("list of")
		)
		return all(requirements)

	while True:
		while topic := get_topic():
			title, short = topic
			if validate(title):
				yield title, short
			else:
				print("Retrying topic generation.")
				continue


def format_sentence(span: Span) -> str:
	"""
	Clean and format a Spacy Span representing a sentence.

	Parameters
	----------
	span : spacy.tokens.span.Span

	Returns
	-------
	sent : str
		A cleaned sentence.
	"""
	sent = span.text.strip()
	sent = re.sub(r"\[.+\]", "", sent)
	sent = re.sub(r"\s+", " ", sent)
	return sent


def raw_text_generator(prompt: str, ai: aitextgen, nlp) -> Callable:
	"""
	Create an callable which generates raw text based on the prompt
	using the GPT2 model passed as an argument.

	Parameters
	----------
	prompt : str

	ai : aitextgen

	Returns
	-------
	generate_raw_text : Callable

	"""
	# Generator for creating random topics.
	topics = topic_generator()
	# List of words that should not be present in the generated text,
	# as that would probably be a sign of overfitting to the prompt.
	forbidden_words = ["slides", "presentation", "powerpoint"]
	# Prompt separator
	separator = "\n-----------------------\n"

	def validate(doc: Doc) -> bool:
		"""
		Validate if the generated document is not too repetitive, too short,
		has too short avg sentences, etc.

		Parameters
		----------
		doc : spacy.tokens.doc.Doc
			Parsed Spacy document to be validated.

		Returns
		-------
		_ : bool
			True means document is ok.
		"""
		if any(word in doc.text.lower() for word in forbidden_words):
			return False
		return True

	def generate_raw_text(max_len=150):
		"""
		Generate text based on a topic.

		Every time this is called, a new random topic will be used.
		"""
		while True:
			print("Generating topic...")
			title, _ = next(topics)
			print(f"**Topic: {title}**")
			print("Generating text, please wait...")
			res = ai.generate_one(
				prompt=prompt.format(title, separator),
				max_length=max_len, temperature=0.8)

			# Get rid of the prompt we added in the beginning.
			text = res.split(separator)[1]
			# Parsing/tagging.
			doc = nlp(text)
			# Validate and retry if needed.
			print("Validating...")
			if validate(doc):
				print("Quality acceptable!")
				return doc, title
			print("Poor quality detected, retrying.")

	return generate_raw_text


def split_into_slides(doc: Doc, slide_max_len: int = 35) -> List[Doc]:
	"""
	Split the sentences into groups, each group to be used as a separate slide.

	Parameters
	----------
	doc : Doc
		The original full document.
	slide_max_len : int
		Maximum number of words per slide.

	Returns
	-------
	slides : List[Doc]
		Partition of the original document.
	"""
	slides = []  # list of merged sub-documents (slides)
	slide_sents = []
	slide_len = 0
	for sent in doc.sents:
		# First, filter out "sentences" which are just empty lines
		# or some artifacts / random floating words.
		if len(sent) < 3:
			continue
		if slide_len + len(sent) > slide_max_len:
			# We have maximum, to avoid overflow we close the existing subdoc,
			# store it, and start a new one
			slides.append(Doc.from_docs(slide_sents))
			slide_sents = []
			slide_len = 0
		# No matter what, we add the current sent to the bucket (which by now
		# might be a fresh one).
		# If it's a fresh one and the current sent is longer than the allowed
		# maximum, then we will end up with a single-sentence slide which
		# may be arbitrarily long.
		slide_sents.append(sent.as_doc())
		slide_len += len(sent)

	# If the iteration finished by adding the last sentence to a bucket, but
	# without exceeding the maximum length, we must still process this bucket.
	if slide_sents:
		slides.append(Doc.from_docs(slide_sents))

	return slides


def get_keywords(doc: Doc, n: int = 3) -> List[str]:
	"""
	Choose a limited number of "interesting" keywords from a doc.

	Parameters
	----------
	doc : Doc
		Spacy document to use as an input text.
	n : int
		How many keywords to choose.

	Returns
	-------
	keywords_sample : List[str]
		List of picked keywords.
	"""
	keywords = set(
		token.lemma_.lower() for token in doc
		if (any(token.pos == pos for pos in (PROPN, NOUN, VERB))
		and not token.is_stop)
	)
	keywords_sample = random.sample(tuple(keywords), n)
	return keywords_sample


def run():
	"""
	Create a kotekian presentation.
	"""
	ai = aitextgen(tf_gpt2="355M")
	nlp = spacy.load("en_core_web_sm")

	prompt = (
		# "Powerpoint presentations consist of several slides with short, descriptive "
		# "and condensed text on each, ideally as bullet points. "
		"A presentation, with 2-3 concise points per slide, follows below. "
		# "It contains simple facts as well as insider knowledge, as a condensed "
		# "list of succinct insights.\n\n"
		"\n\n"
		"Presentation topic:\n{}{}"
	)
	generate_raw_text = raw_text_generator(prompt, ai, nlp)
	rawdoc, title = generate_raw_text(max_len=400)
	print("Generation finished:")
	print(f"Title: {title}")
	print(f"Text:\n{rawdoc}")
	print("-------")
	print("Splitting text into slides...")
	text_slides = split_into_slides(rawdoc, 60)
	print(text_slides)
	presentation = beamer.Presentation(title)
	for slide in text_slides:
		keywords = get_keywords(slide)
		img = graphics.download_random_image(keywords)
		presentation.add(
			title="Niaou",
			points=list(slide.sents),
			image=img
		)

	fname = beamer.generate_tex(presentation)
	_ = beamer.render_pdf(fname)


if __name__ == "__main__":
	run()
